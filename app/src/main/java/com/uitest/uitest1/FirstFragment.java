package com.uitest.uitest1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.view.KeyEvent;
import android.widget.Toast;


public class FirstFragment extends Fragment {
    EditText first_input = null;
    EditText second_input = null;

    private static String last_text = "";
    private static String second_last_text = "";


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.first_fragment, container, false);

        Log.d("INPU0988888888888","0: INPUT1: ");


       first_input = view.findViewById(R.id.first_input);
        second_input = view.findViewById(R.id.second_input);
        first_input.setText("");
        second_input.setText("");




        switch (MainActivity.first_fragment_last_input_view)
        {

            case MainActivity.FIRST_FRAGMENT_INPUT1_VIEW:



                    first_input.setText(MainActivity.first_fragment_input1_text );
                Log.d("FRAGMEN11","1 1: INPUT1: " + first_input.getText().toString() + " INPUT2: " + second_input.getText().toString());

                break;

            case MainActivity.FIRST_FRAGMENT_INPUT2_VIEW:


                    second_input.setText(MainActivity.first_fragment_input2_text );
                Log.d("FRAGMEN11","1 2: INPUT1: " + first_input.getText().toString() + " INPUT2: " + second_input.getText().toString());


                break;

        }


        first_input.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {


                    MainActivity.first_fragment_last_input_view = MainActivity.FIRST_FRAGMENT_INPUT1_VIEW;

                    last_text = s.toString();



            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {



            }
        });

        second_input.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {





                     MainActivity.first_fragment_last_input_view = MainActivity.FIRST_FRAGMENT_INPUT2_VIEW;

               

                last_text = s.toString();



            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {





            }
        });





        return view;




    }


    @Override
    public void onResume() {
        super.onResume();




    }





    @Override
    public void onPause() {
        super.onPause();
        



        MainActivity.second_last_input_text = MainActivity.last_input_text;;
     
     
        MainActivity.last_input_text = last_text;


        if (first_input != null) {

            String input1 = first_input.getText().toString();

         
            if (input1.equals("") == false) {

                MainActivity.first_fragment_input1_text = input1;
            }
        }

        if (second_input != null) {
            String input2 = second_input.getText().toString();


            if (input2.equals("") == false) {

                MainActivity.first_fragment_input2_text = input2;
            }
        }

      
      




    }




    @Override
    public void onDestroy() {
        super.onDestroy();


    }






}
