package com.uitest.uitest1;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ThirdFragment extends Fragment {

    TextView first_view = null;
    TextView second_view = null;
    TextView multi_view = null;

    int first = 0;
    int second = 0;
    int third = 0;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.third_fragment, container, false);


        first_view = view.findViewById(R.id.first_view);
        second_view = view.findViewById(R.id.second_view);
        multi_view = view.findViewById(R.id.multiplied_view);

        multi_view.setText("MULTI1 :");


        first_view.setText("FIRST: "  + MainActivity.last_input_text);

        second_view.setText("SECOND: "  + MainActivity.second_last_input_text);


        if (MainActivity.last_input_text.equals("") == false && MainActivity.second_last_input_text.equals("") == false) {

           first = Integer.parseInt(MainActivity.last_input_text);
            second = Integer.parseInt(MainActivity.second_last_input_text);

           third = first * second;

            multi_view.setText("MULTIPLIED: "+ third + "");
        }






        return view;




    }


    @Override
    public void onPause() {
        super.onPause();

        MainActivity.third_fragment_input1_text = first + "";
        MainActivity.third_fragment_input2_text = second + "";
        MainActivity.third_fragment_input2_text = third + "";


    }

    @Override
    public void onResume() {
        super.onResume();


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

    }
}
