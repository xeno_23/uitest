package com.uitest.uitest1;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    public static String first_fragment_input1_text = "";
    public static String first_fragment_input2_text = "";
    public static String second_fragment_input1_text = "";
    public static String second_fragment_input2_text =" ";
    public static String third_fragment_input1_text = "";
    public static String third_fragment_input2_text = "";
    public static String third_fragment_input3_text = "";


    public static final int FIRST_FRAGMENT = 0;
    public static final int SECOND_FRAGMENT = 1;
    public static final int THIRD_FRAGMENT = 2;

    public static int init_fragment = FIRST_FRAGMENT;
    public static final int INIT_VALUE = 999;

    public static final int FIRST_FRAGMENT_INPUT1_VIEW = 0;
    public static final int FIRST_FRAGMENT_INPUT2_VIEW = 1;
    public static final int SECOND_FRAGMENT_INPUT1_VIEW = 2;
    public static final int SECOND_FRAGMENT_INPUT2_VIEW = 3;

    public static int first_fragment_last_input_view = INIT_VALUE;
    public static int second_fragment_last_input_view = INIT_VALUE;

    public static String last_input_text = "";
    public static String second_last_input_text = "";
    TabLayout tabLayout;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /**
         * TabLayout to implement swithc between views
         */

        tabLayout = findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("VIEW 1"));
        tabLayout.addTab(tabLayout.newTab().setText("VIEW 2"));
        tabLayout.addTab(tabLayout.newTab().setText("VIEW 3"));
        tabLayout.setTabGravity(TabLayout.INDICATOR_GRAVITY_TOP);



        /**
         * Views' past states
         */
        SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        init_fragment = saved_values.getInt(getString(R.string.init_fragment), FIRST_FRAGMENT);



        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {


                init_fragment = tab.getPosition();

                selectFragment(init_fragment);



            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {





            }
        });




    }



    /**
     * Method for selecting current Fragment
     */
    void selectFragment(int i)
    {



        switch(i)
        {

            case FIRST_FRAGMENT: {

                FirstFragment f = new FirstFragment();

                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.fragment_container, f);
                transaction.commit();

            }
            break;





            case SECOND_FRAGMENT: {

                SecondFragment f = new SecondFragment();

                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.fragment_container, f);
                transaction.commit();

            }
            break;


            case THIRD_FRAGMENT: {

                ThirdFragment f = new ThirdFragment();

                FragmentManager manager = getSupportFragmentManager();
                FragmentTransaction transaction = manager.beginTransaction();
                transaction.replace(R.id.fragment_container, f);
                transaction.commit();

            }
            break;

        }

    }


    @Override
    protected void onResume() {
        super.onResume();



        /**
         * Views' past states that were saved when onPause was called
         */


        SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        init_fragment = saved_values.getInt(getString(R.string.init_fragment), FIRST_FRAGMENT);




        first_fragment_input1_text = saved_values.getString(getString(R.string.first_fragment_input1_text),"");
        second_fragment_input1_text = saved_values.getString(getString(R.string.second_fragment_input1_text),"");
        third_fragment_input1_text = saved_values.getString(getString(R.string.third_fragment_input1_text),"");

        first_fragment_input2_text = saved_values.getString(getString(R.string.first_fragment_input2_text),"");
        second_fragment_input2_text = saved_values.getString(getString(R.string.second_fragment_input2_text),"");
        third_fragment_input2_text = saved_values.getString(getString(R.string.third_fragment_input2_text),"");

        third_fragment_input3_text = saved_values.getString(getString(R.string.third_fragment_input3_text),"");




       selectFragment(init_fragment);


        TabLayout.Tab tab = tabLayout.getTabAt(init_fragment);
        tab.select();



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onPause() {
        super.onPause();



        /**
         * Views' current states are saved to shared preferences
         */

        SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor=saved_values.edit();
        editor.putInt(getString(R.string.init_fragment),init_fragment);

        editor.putString(getString(R.string.first_fragment_input1_text),first_fragment_input1_text);
        editor.putString(getString(R.string.first_fragment_input2_text),first_fragment_input2_text);
        editor.putInt(getString(R.string.first_fragment_last_input_view),first_fragment_last_input_view);

        editor.putString(getString(R.string.second_fragment_input1_text),second_fragment_input1_text);
        editor.putString(getString(R.string.second_fragment_input2_text),second_fragment_input2_text);
        editor.putInt(getString(R.string.second_fragment_last_input_view),second_fragment_last_input_view);

        editor.putString(getString(R.string.third_fragment_input1_text),third_fragment_input1_text);
        editor.putString(getString(R.string.third_fragment_input2_text),third_fragment_input2_text);
        editor.putString(getString(R.string.third_fragment_input3_text),third_fragment_input3_text);



        editor.commit();



        init_fragment = saved_values.getInt(getString(R.string.init_fragment), THIRD_FRAGMENT);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


        /**
         * When app closes it's Frament's current states are saved
         */

        SharedPreferences saved_values = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor=saved_values.edit();

        editor.putString(getString(R.string.first_fragment_input1_text),first_fragment_input1_text);
        editor.putString(getString(R.string.first_fragment_input2_text),first_fragment_input2_text);
        editor.putInt(getString(R.string.first_fragment_last_input_view),first_fragment_last_input_view);

        editor.putString(getString(R.string.second_fragment_input1_text),second_fragment_input1_text);
        editor.putString(getString(R.string.second_fragment_input2_text),second_fragment_input2_text);
        editor.putInt(getString(R.string.second_fragment_last_input_view),second_fragment_last_input_view);

        editor.putString(getString(R.string.third_fragment_input1_text),third_fragment_input1_text);
        editor.putString(getString(R.string.third_fragment_input2_text),third_fragment_input2_text);
        editor.putString(getString(R.string.third_fragment_input3_text),third_fragment_input3_text);


        editor.putInt(getString(R.string.init_fragment),FIRST_FRAGMENT);
        editor.commit();


        SharedPreferences saved_values2 = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        init_fragment = saved_values2.getInt(getString(R.string.init_fragment), FIRST_FRAGMENT);




    }










    boolean doubleBackToExitPressedOnce = false;

    @Override
    public void onBackPressed() {



        /**
         * Checks weather user wants to close the app
         */


        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }



}
